package me.justnaaa.dramaProject;

import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import spark.ModelAndView;
import spark.template.mustache.MustacheTemplateEngine;

import java.util.HashMap;
import java.util.Map;

import  static spark.Spark.*;

public class main {

    private static Map root = new HashMap();
    private static Map person = new HashMap();
    private static Map play = new HashMap();

    public static void main(String[] args) {
        port(80);
        Jedis j = new Jedis("localhost");
        // Root
        get("/", (req, res) -> {
            String project = "drama";
            JSONArray menu = new JSONArray(j.get(project + "menu");
            StringBuilder list = new StringBuilder();
            // Creating the list
            for (int i = 0; <= menu.length();) {
                list.append("<h1>" + menu[i] + "</h1><br>");
                i++;
            }
            root.put("list", list.toString());
        })
        // Person
        get("p/:person", (req, res) -> {
            JSONObject pJson = new JSONObject(j.get(req.params("person")));
            person.put("person", pJson.getString("name"));
            person.put("deathAge", pJson.getString("ageOfDeath"));
            person.put("sources", pJson.getString("sources"));
            StringBuilder achivemts = new StringBuilder();
            // Achievements
            for (int i = 0  ; i < pJson.getJSONArray("achivements").length();) {
                achivemts.append(", ").append(pJson.getJSONArray("achivements").getString(i));
                i++;
            }
            person.put("achivments", achivemts);
            // Plays
            StringBuilder pplays = new StringBuilder();
            for (int i = 0  ; i < pJson.getJSONArray("plays").length();) {
                pplays.append(", ").append(pJson.getJSONArray("plays").getString(i));
                i++;
            }
            person.put("plays", pplays);
            return new MustacheTemplateEngine().render(new ModelAndView(person, "person.mustache"));
        });
        get("pl/:play", (req, res) -> {
            JSONObject pJson = new JSONObject(j.get(req.params("play")));
            play.put("name", pJson.getString("name"));
            play.put("about", pJson.getString("about"));
            play.put("sources", pJson.getString("sources"));
            StringBuilder plot = new StringBuilder();
            for (int i = 0  ; i < pJson.getJSONArray("points").length();) {
                plot.append(pJson.getJSONArray("points").getString(i));
                i++;
            }
            play.put("plot", plot);
            return new MustacheTemplateEngine().render(
                    new ModelAndView(play, "play.mustache")
            );
        });
    }
}
